
<!DOCTYPE html>
<html lang="en">
<head>
  <title>nsewtech</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="margin-top:100px;">
  <h2 style="text-align:center;">User Data</h2>   
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Contact</th>
        <th>Email</th>
        <th>Register Date </th>
      </tr>
    </thead>
       
    <?php

include ('../connection.php');

$query = "select * from contact order by id desc";

$response = mysqli_query($conn,$query);

while($re = mysqli_fetch_array($response))
{
   
    echo '<tr>
    <td>'.$re['id'].'</td>
    <td>'.$re['name'].'</td>
    <td>'.$re['contact'].'</td>
    <td>'.$re['email'].'</td>
    <td>'.$re['registerdate'].'</td>
  </tr>';


}

mysqli_close($conn);
?>

    <tbody>
    
    </tbody>
  </table>
</div>

</body>
</html>
